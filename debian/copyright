Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Jimmy Brisson <jimmy.brisson@arm.com>
Upstream-Name: mbed-ls
Source: https://github.com/ARMmbed/mbed-ls
Files-Excluded:
 .gitignore
 .travis.yml
 mbed_lstools/darwin.py
 mbed_lstools/windows.py
 test/os_darwin.py
 test/os_win7.py
 test/test_data/lpc1768/*.bin
Comment: The following files were removed:
 .gitignore                   : upstream gitignore
 .travis.yml                  : upstream CI config
 mbed_lstools/darwin.py       : Win7-specific support
 mbed_lstools/windows.py      : MacOS X-specific support
 test/os_darwin.py            : MacOS-specific tests
 test/os_win7.py              : Win7-specific tests
 test/test_data/lpc1768/*.bin : Pre-compiled NXP 1768 bin files

Files: *
Copyright: 2011-2015 ARM Limited
License: Apache-2.0

Files: mbed_lstools/platform_database.py
Copyright: 2017 ARM Limited
License: Apache-2.0

Files: debian/*
Copyright: 2018-2024 Nick Morrott <nickm@debian.org>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache License version 2.0
 can be found in the file `/usr/share/common-licenses/Apache-2.0'.
